//Express JS
/*
	Create an API using a NODEJS Framework


NODE JS Review

An open-source, Javascript runtime envoronment for creating server-side applications.


Runtime Environment.
- Gives the context for running a programming language
- JS was initially within the context of the browser, giving it access to:
	* DOM
	* window object
	etc.
- with Node.js the contxt was taken out of the browser and put into the server.


Benifits
- Performance
- Familiar
- Access to Node Package Manager (NPM)
NPM: https://docs.npmjs.com/packages-and-modules

Limitations
- Node.js does not have direct support for common web development tasks such as:
	* handling HTTP verbs (GET,POST,DELETE etc)
	* Handling requests at different URL (Routing)
	* Serving of static files
- This means that using Node.js on its own for development may get tedious.


Instead of writing the code foe thses common web development tasks yourself, save your time and effort by using Express.js.


WEB FRAMEWORK
- A set of components designed to simplify the web dev process.
	* Convention over configuration
- Allows devs to focus on the business logic of their app
- Comes in two forms: opinionated and unponionated.


Opinionated Web Framework
- The framework dictates how it should be used by the dev.
- Speeds up the stratup process of app development
- Enforces best practices for the framework's use case
- Lack of flexibility could be a drawback when app's needs are not aligned with the framework's assumptions.

Unopinionated Web Framework
- The dev dictates how to use the framework.
- Offers flexibility to create an application unbound by any use case.
- No "right way" of structuring an application.
- Abudance of options may be overwhelming.


Advatages (over plain Node.js)
- Simplicity makes it easy to learn and use
- offers ready to use components for the most common web
*/



//Code-along
/*
- in our terminal we type "npm init"
para makagawa ng package.
- after nyan nag type tayo ng "npm install express
" para ma install ang express.JS

https://www.cloudflare.com/learning/network-layer/what-is-a-computer-port/
*/


/*
 - Use the "require" directive to lead the express module/package.
 - A "module" is a software component or part of a program that contains one or more routines.
 - This is used to get the contects of the packages to be used by our application.
 - It also allows us to access methods and functions that will allow us to easily create a server.
*/
const express = require("express");

/*
- Create an application using express.
- This creates an express application and stores this in a constant called app
- In layman's term, app is our server.
*/
const app = express();

/*
- For our application server to run, we need a port to listen to.
*/
const port = 3000;

//MIDDLEWARES
/*
- Set up for allowing the server to handle data from requests.
- Allows your app to read json data
- Methods used from express.js are middlewares
- Middleware is a layer of software that enables interaction and transmission of information between assorted applications.
*/
app.use(express.json());

/*
- Allows your app to read data from forms.
- By default, information received from the URL can only be received as a string or an array.
- By applying the option of "extended:true", we are allowed to receive information in other data types such as an object throughout our application.
*/
app.use(express.urlencoded({extended: true}));


//ROUTES
/*
- Express has methods corresponding to each HTTP method
- This route expects to receive a GET request at the base URI.
- The full base URL for our local application for this route will be at "http://localhost:3000"
*/

//RETURNS SIMPLE MESSAGE
/*
	- This route expexts to receive a GET request at the base URI "/"

	POSTMAN:
	url: http://localhost:3000/
	method: GET
*/
app.get("/", (request, response) => {
	response.send("Hello World");
});

//RETURNS SIMPLE MESSAGE
/*
	URI: /hello
	method: GET

	POSTMAN:
	url: http://localhost:3000/hello
	method: GET
*/
app.get("/hello", (request, response) => {
	response.send(`Hello from the "/hello" endpoint`);
});

//RETURNS SIMPLE GREETING
/*
	uri: /hello
	method: POST

	POSTMAN:
	URL: http://localhost:3000/hello
	method: POST
	body: raw + json
		{
			"firstName": "Abdul",
			"lastName": "Ducusin"
		}
*/
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body} ${request.body.lastName}! This is from the "/hello" endpoint but with a post method`);
})



//REGISTER USER ROUTE
/*
	- This route expects to receive a POST request at the URI "/register"
	- This will create a user object in the "users" variable that mirrors a real world registration process.

	URI: /hello
	method: 'POST'

	POSTMAN:
	url: http://localhost:3000/register
	method: 'POST'
	body: raw + json
		{
			"username": "Abdul",
			"password": " "
		}
*/
let users = [];

app.post("/register", (request, response) => {
	
	if(request.body.username !== " " && request.body.password !== " "){
		users.push(request.body);
		response.send(`User ${request.body.username} sucessfully registered!`)
		console.log(request.body)
	} else {
		response.send(`please input BOTH username and password`)
	}

})


//CHANGE PASSWORD ROUTE
/*
	- This route expects to receive a PUT request at the URI "/change-password"
	- This will update the password of a user that matches the information provided in the client/postman.

	URI: /change-password
	method: PUT

	POSTMAN
	url: http://localhost:3000/change-password
	method: PUT
	body: raw + json
	{
		"username": "AJ",
		"password": "AJ9876"
	}
*/
app.put("/change-password", (request, response) => {

	//Create a variable to store the message to be sent back to the client/Postman
	let message ;
	console.log("Woks after the message");


	//Creates a loop taht will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++){

		// If the username provided in the client/Postman and the username of the current object in the loop is the same
		console.log("Woks before the if");
		if(request.body.username == users[i].username){

			// Changes the password of the user found by the loop into the password provided in the client/Postman
			users[i].password = request.body.password

			// Changes the message to be sent if the password has been updated
			message = `Users ${request.body.username}'s password has been updated`

			// Breaks out of the loop once a user that matches the username provided in the client/Postman is found
			break;

		// If no user is found, else
		} else {

			// Changes the message to be sent back by the response
			message = `Users does not exist`
		}
	}
	console.log("Woks before the response send");
	response.send(message);

})




















/*
	- Tells our server to listen to the port
	- If the port is accessed, we can run the server
	- Returns a message to confirm that the server is running in the terminal.
*/
app.listen(port, () => console.log(`Server running at ${port}`));